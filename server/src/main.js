const express = require(`express`);
const bodyParser = require(`body-parser`);

const app = express();

app.use(bodyParser.json({}));

function addHeaders(res) {
  res.set(`allow`, `OPTIONS, GET, POST`);
  res.set(`content-type`, `application/json`);
  res.set(`vary`, `Origin`);
  res.set(`access-control-allow-headers`, `content-type`);
  res.set(`access-control-allow-methods`, `OPTIONS, GET, POST`);
  res.set(`access-control-allow-origin`, `*`);
}

function sendResponse(res, statusCode, data = {}) {
  const body = JSON.stringify(data);
  addHeaders(res);
  res.status(statusCode).end(body);
}

app.get(`/health-check`, (_, res) => sendResponse(res, 200));

app.options(`/`, (_, res) => {
  console.log(`Received an OPTIONS request`);
  sendResponse(res, 204);
});

app.get(`/`, (_, res) => {
  console.log(`Received a GET request`);
  const reply = `Send a POST request to this endpoint to see what happens!`;
  sendResponse(res, 200, { reply });
});

app.post(`/`, (req, res) => {
  console.log(`Received a POST request`);
  const reply = `You sent me: "${req.body.msg}"`;
  sendResponse(res, 200, { reply });
});

app.listen(process.env.PORT, () =>
  console.log(`Server listening on port ${process.env.PORT}!`)
);
