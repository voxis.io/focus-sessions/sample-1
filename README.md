# sample-1

Sample repository for focus group session.

## Instructions

1. Install Node.js with nvm: https://github.com/nvm-sh/nvm#installing-and-updating
2. Install Voxis CLI: `npm install -g @voxis/cli`
3. Fork sample repo: https://gitlab.com/voxis.io/focus-sessions/sample-1
4. Git clone the forked repo locally.
5. Create an account: `vox register`
6. Login: `vox login <EMAIL>`
7. Login to chosen VCS: `vox vcs.login gitlab`
8. Create an app: `vox create`
9. Open the `./client/client.voxis.yaml` config file.
10. Set the `lifecycle` field to "static" like:
11. Open the `./server/server.voxis.yaml` config file.
12. Set the `lifecycle` field to "persistent" like;
13. Add the build and execute config like:

```yaml
build:
  image: node:14.8.0-alpine3.12
  command: npm install
  outputDir: ./
execute:
  command: npm start
```

14. Git commit and push the Voxis config files.
15. Trigger the first deployment: `vox deploy`.
16. Check the status of deployment: `vox status`
17. **BUG WORKAROUND**: Must redeploy a second time: `vox deploy --force`.
18. Get the endpoint for the client: `vox status <APP> <SERVICE>`
19. Visit the client endpoint in browser: `open https://<SERVICE_ENDPOINT>`
20. Try to send a message to the server.
21. Get the endpoint for the server: `vox status <APP> <SERVICE>`
22. Make a GET request to the endpoint: `curl https://<ENDPOINT>`
23. Open `./client/js/script.js` and set the `SERVER_URL` variable to the endpoint for the server.
24. Git commit & push.
25. Check the status of deployment: `vox status`
26. Open the client page again and try sending another message to the server.
27. Add a domain to your account: `vox domain.add <FQDN>`
28. Open the `./client/client.voxis.yaml` config file again.
29. Add the domain in `deploy.prod.domains` like:

```yaml
deploy:
	prod:
		ref: branch/master
		domains:
			- <FQDN>
```

30. Git commit & push.
31. Check the status of deployment: `vox status`
32. Get DNS records with `vox domain.setup <FQDN>`
33. Update DNS records for <FQDN> with IP.
34. Check status of domain: `vox domain <FQDN>`
35. Visit custom domain to verify it works: `open https://<FQDN>`
